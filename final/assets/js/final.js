var CurrentNotebook = 'Search';	
var PlaylistQueue   = [];       
var PlaylistIndex   = -1;   

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

function displayTab() {

    
	var tabs = document.getElementsByClassName('nav-tabs')[0].getElementsByTagName('li');
	for (var i = 0; i < tabs.length; i++) {
	    var tab = tabs[i];
	    tab.classList.remove('active');
	}
	this.classList.add('active');

	var renderHTML = ''; 
 
	switch (this.id) {
	    case 'search':
	    	displaySearch();
			break;
	    case 'animals':
            displayAnimal();
			break;
        case 'playlist':
            break
		}	
		
	    
}

function displayResults(url) {
    requestJSON(url, function(data) {
    
        if (data['render'] == 'gallery') {
            renderEnvironment(data['results'], data['prefix']);
        } else if (data['render'] == 'animal') {
            renderAnimals(data['results']);
        } 
    });
}


function displaySearch(query) {

	CurrentNotebook = 'Search';

	var element    = document.getElementById('notebook-body-nav');
    var renderHTML = '<form class="navbar-form text-center">';

    renderHTML += '<div class="form-group">';
    renderHTML += '<label class="control-label sr-only">Title</label>';
    renderHTML += '<input id="query" type="text" name="query" class="form-control" placeholder="Find an animal!" onkeyup="updateSearchResults()"></div>';
    renderHTML += '<div class="form-group">';
    renderHTML += '<select id="table" name="table" class="form-control" onchange="updateSearchResults()">'
    renderHTML += '<option value="Animals">Animals</option>';
    renderHTML += '</div></form>';

    element.innerHTML = renderHTML;
    return updateSearchResults();
 } 

function displayEnvironment(environment) {
	CurrentNotebook = 'Environments'

    if (environment === undefined) {
        environment = ''
    } 
    displayResults('/environment/' + environment)
}

function displayAnimal(animal) {
	CurrentNotebook = 'Animals'

    if (animal === undefined) {
        animal = ''
    } 
    displayResults('/animal/' + animal)
}

function displayFact(fact) {
	CurrentNotebook = 'Facts'

    if (fact === undefined) {
        fact = ''
    } 
    displayResults('/fact/' + fact)

}

 

function renderEnvironment(data, prefix) {
    
    var element  = document.getElementById('notebook-body-contents');
    var renderHTML = '<div class="row">';

     for (var i=0; i<data.length; i++) {

        
        var item    = data[i]
        var id      = item[0]
        var animal  = item[1]
        var image   = item[2]

       

        if (i > 0 && i % 4 == 0) {
            renderHTML += '</div><div class="row">';
        }

        renderHTML += '<div class="col-xs-3">';
        renderHTML += '<a href="#" onclick="display' + prefix + '(' + id + ')" class="thumbnail text-center" >';
        renderHTML += '<img src="' + image + '"width=250 height=250>';
        renderHTML += '<div class="caption"><h4>' + animal + '</h4></div>';
        renderHTML += '</a></div>';
    }

    renderHTML += '</div>';
    element.innerHTML = renderHTML;

}

function renderAnimals(data) {
    
    var element  = document.getElementById('notebook-body-contents');
    var renderHTML = '<table class="table table-striped">';
  
    renderHTML += '<thead><th>Number</th><th>Creatures Various Sounds</th></thead><tbody>';

    for (var i=0; i<data.length; i++) {
        var item   = data[i]
        var envid     = item[0]
        var animalid = item[1]
        var mp4   = item[2]

        

        renderHTML += '<tr><td>' + animalid + '</td>';
        renderHTML += '<td><video width="320" height="240" src="/assets/mp3/' + mp4 + '"controls></video>';
        renderHTML += '</td></tr>';



        
    }

    renderHTML += '</tbody></table>';        

    element.innerHTML = renderHTML;
}



function updateSearchResults() {
    
     var query   = document.getElementById('query').value
     var table   = document.getElementById('table').value

     url = '/search?query=' + query + '&table=' + table;
     displayResults(url)
}




window.onload = function() {
	var tabs = document.getElementsByClassName('nav-tabs')[0].getElementsByTagName('li');
	for (var i = 0; i < tabs.length; i++) {
	    var tab = tabs[i];
	    tab.onclick = displayTab;
	}
    

	displaySearch()

	
}