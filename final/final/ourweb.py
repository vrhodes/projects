

from final.db import Database

import json
import logging
import socket
import socket

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9999
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        self.logger   = logging.getLogger()
        self.database = Database()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.port     = port

    
        self.add_handlers('', [
            (r'/'           , IndexHandler),
            (r'/search'     , SearchHandler),
            (r'/environment/(.*)', EnvHandler),
            (r'/animal/(.*)'     , AnimalHandler),
            (r'/fact/(.*)'       , FactHandler),
            (r'/sound/(.*)' , SoundHandler),
            (r'/assets/(.*)', tornado.web.StaticFileHandler, {'path': 'assets'}),
        ])

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()



# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('final.html')

class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Implement Index Handler
        query   = self.get_argument('query', '')
        table   = self.get_argument('table', '')
        results = []

        if table == 'Environments':
            results = self.application.database.environments(query)
        elif table == 'Animals':
            results = self.application.database.animals(query)
        elif table == 'Facts':
            results = self.application.database.facts(query)

        self.write(json.dumps({
            'render' : 'gallery',
            'prefix' : table[:-1],
            'results': list(results),
        }))

class EnvHandler(tornado.web.RequestHandler):
    def get(self, env_id=None):
        if not env_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Environment',
                'results': list(self.application.database.environments('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Animal',
                'results': list(self.application.database.environment(env_id)),
            }))

class AnimalHandler(tornado.web.RequestHandler):
    def get(self, animal_id=None):
        if not animal_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Animal',
                'results': list(self.application.database.animals('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'animal',
                'results': list(self.application.database.animal(animal_id)),
            }))

class FactHandler(tornado.web.RequestHandler):
    def get(self, fact_id=None):
        if not fact_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Fact',
                'results': list(self.application.database.facts('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'fact',
                'results': list(self.application.database.fact(fact_id)),
            }))
class SoundHandler(tornado.web.RequestHandler):
    def get(self, facts_id=None):
        self.write(json.dumps({
            'sound': self.application.database.sound(facts_id),
        }))
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
