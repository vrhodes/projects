
import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'final.db'
    YAML_PATH   = 'assets/yaml/data.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(self.path)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        environments_sql = '''
        CREATE TABLE IF NOT EXISTS Environments (
            EnvID       INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            Image       TEXT    NOT NULL,
            PRIMARY KEY (EnvID)
        )
        '''

        animals_sql = '''
        CREATE TABLE IF NOT EXISTS Animals (
            EnvID       INTEGER NOT NULL,
            AnimalID    INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            Image       TEXT    NOT NULL,
            PRIMARY KEY (AnimalID),
            FOREIGN KEY (EnvID) REFERENCES Environments(EnvID)
        )
        '''

        facts_sql = '''
        CREATE TABLE IF NOT EXISTS Facts (
            AnimalID    INTEGER NOT NULL,
            FactID      INTEGER NOT NULL,
            Number      INTEGER NOT NULL,
            Fact        TEXT NOT NULL,
            PRIMARY KEY (FactID),
            FOREIGN KEY (AnimalID) REFERENCES Animals(AnimalID)
        )
        '''

        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute(environments_sql)
            cursor.execute(animals_sql)
            cursor.execute(facts_sql)

    def _load_tables(self):
        environments_sql = 'INSERT OR REPLACE INTO Environments (EnvId, Name, Image) VALUES (?, ?, ?)'
        animals_sql      = 'INSERT OR REPLACE INTO Animals (EnvID, AnimalID, Name, Image) VALUES (?, ?, ?, ?)'
        facts_sql        = 'INSERT OR REPLACE INTO Facts (AnimalID, FactID, Number, Fact) VALUES (?, ?, ?, ?)'

        with self.conn:
            cursor    = self.conn.cursor()
            environments_id = 1
            animals_id      = 1
            facts_id        = 1

            for environment in yaml.load(open(self.data)):
                cursor.execute(environments_sql, (environments_id, environment['name'], environment['image']))
                self.logger.debug('Added Environment: id={}, name={}'.format(environments_id, environment['name']))

                for animal in environment['animals']:
                    cursor.execute(animals_sql, (environments_id, animals_id, animal['name'], animal['image']))
                    self.logger.debug('Added Animal: id={}, name={}'.format(animals_id, animal['name']))

                    for fact_number, fact in enumerate(animal['fact']):
                        cursor.execute(facts_sql, (animals_id, facts_id, fact_number + 1, fact))
                        self.logger.debug('Added Fact: id={}, name={}'.format(facts_id, fact))
                        facts_id += 1

                    animals_id += 1

                environments_id += 1

    def environments(self, environment):
        environments_sql = 'SELECT EnvID, Name, Image FROM Environments WHERE Name LIKE ? ORDER BY Name'
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(environments_sql, ('%{}%'.format(environment),))

    def environment(self, env_id=None):
        environment_sql = '''
        SELECT      AnimalID, Name, Image
        FROM        Animals
        WHERE       EnvID = ?
        ORDER BY    Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(environment_sql, (environments_id,))

    def animals(self, animal):
        animals_sql = 'SELECT AnimalID, Name, Image FROM Animals WHERE Name LIKE ? ORDER BY Name'
        
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(animals_sql, ('%{}%'.format(animal),))

    def animal(self, animal_id=None):
        animal_sql = '''
        SELECT      FactID, Number, Fact
        FROM        Facts
        WHERE       AnimalId = ?
        ORDER BY    Number
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(animal_sql, (animal_id,))

    def facts(self, fact):
        facts_sql = '''
        SELECT      Facts.FactID, Facts.Fact, Image
        FROM        Facts
        JOIN        Animals
        ON          Facts.AnimalID = Animals.AnimalID
        WHERE       Facts.Fact LIKE ?
        ORDER BY    Facts.FactID
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(facts_sql, ('%{}%'.format(fact),))

    def fact(self, fact_id=None):
        fact_sql = '''
        SELECT      FactID, Environments.EnvID, Environments.Name, Animals.AnimalID, Animals.Name, Number, Facts.Fact, Animals.Image
        FROM        Facts
        JOIN        Animals
        ON          Animals.AnimalID = Facts.AnimalID
        JOIN        Environments
        ON          Environments.EnvID = Animals.EnvID
        WHERE       FactID = ?
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(fact_sql, (facts_id,)).fetchone()

    def sound(self, facts_id=None):
            # TODO: Select specific song information
            facts_sql = '''
            SELECT      Facts.FactID, Environments.Name, Animals.Name, Animals.Image, FactID
            FROM        Facts
            JOIN        Animals
            ON          Animals.AnimalID = Facts.AnimalID
            JOIN        Environments
            ON          Environments.EnvID = Animals.AnimalID
            WHERE       FactID = ?
            '''

            with self.conn:
                cursor = self.conn.cursor()
                funStuff = cursor.execute(facts_sql, (facts_id,)).fetchone()

            soundInfo = {}
            soundInfo['Fact']  = funStuff[0]
            soundInfo['EnvironmentName'] = funStuff[1]
            soundInfo['AnimalsName']  = funStuff[2]
            soundInfo['AnimalsImage'] = funStuff[3]
            soundInfo['soundURL']    = '/assets/mp3/{:04d}.mp3'.format(int(funStuff[4]))

            return soundInfo
    # vim: set sts=4 sw=4 ts=8 expandtab ft=python:
