#!/usr/bin/env python

from drawit import Image, Color, Point, PPM, BLACK, WHITE, YELLOW, BLUE, GREEN, RED, MAGENTA
import math

class Logo(Image):

	def draw_background(self):
		self.draw_rectangle(Point(0, 0), Point(self.width - 1, self.height - 1), BLACK)

	def draw_star(self):
		center = Point(self.width / 15, self.height / 15)
		radius = min(self.width, self.height) / 15 - 1

		self.draw_circle(center, int(radius/2.5), WHITE)

	def draw_star_two(self):
		center = Point(self.width / 10, self.height / 10)
		radius = min(self.width, self.height) / 10 - 1

		self.draw_circle(center, int(radius/2.25), BLUE)

	def draw_star_three(self):
		center = Point(self.width / 7, self.height / 7)
		radius = min(self.width, self.height) / 7 - 1

		self.draw_circle(center, int(radius/2), GREEN)

	def draw_star_four(self):
		center = Point(self.width / 5, self.height / 5)
		radius = min(self.width, self.height) / 5 - 1

		self.draw_circle(center, int(radius/1.75), MAGENTA)

	def draw_star_five(self):
		center = Point(self.width / 3, self.height / 3)
		radius = min(self.width, self.height) / 3 - 1

		self.draw_circle(center, int(radius/1.5), RED)

	def draw_star_six(self):
		center = Point(self.width / 2, self.height / 2)
		radius = min(self.width, self.height) / 2 - 1

		self.draw_circle(center, int(radius/1.25), YELLOW)

if __name__ == '__main__':
	logo = Logo()
	logo.draw_background()
	logo.draw_star()
	logo.draw_star_two()
	logo.draw_star_three()
	logo.draw_star_four()
	logo.draw_star_five()
	logo.draw_star_six() 

PPM.write(logo)