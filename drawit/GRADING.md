Project 01 - Grading
--------------------

Group Score: 26.5/27

Feedback on code quality:

-0.5 For color.py __eq__ function, you should only return true if all three colors are equal. The way you have it written now, it will return true if only one of the colors is equal. It should also explicitly return false whenever the colors are not equal. The same is true for the __eq__ functions for point.py and image.py

Other than that, your code looks good. Maybe consider adding a few more comments to explain what is going on, especially in a function like draw_line where there is some math being done.


Individual Work:

Vrhodes: 3/3

Jjacks14: 0/3

Feedback on individual code quality:

Vrhodes - Good work. One suggestion is to try not to hard-code values for points. Instead, try making your points relative to the size of the image by making them percentages of the image width or height. That way, if your image size changes, you do not have to recalculate and change every value.
