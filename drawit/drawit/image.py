from color import Color
from point import Point

import math

class Image(object):
    DEFAULT_WIDTH  = 640
    DEFAULT_HEIGHT = 480

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ''' Initialize Image object's instance variables '''
        self.width = width 
        self.height = height
        self.pixels = []

        for row in range(height):
            pixelrow = []
            for column in range(width):
                pixelrow.append(Color())

            self.pixels.append(pixelrow)



    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        if self.pixels == other.pixels: 
            return True 


    def __str__(self):
        ''' Returns string representing Image object '''
        return 'Image(width={},height={})'.format(self.width, self.height) #fix

    def __getitem__(self, point):
        ''' Returns pixel specified by point

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        if point.x in range(self.width) and point.y in range(self.height):
            return self.pixels[point.y][point.x]
        else:
            raise IndexError

    def __setitem__(self, point, color):
        ''' Sets pixel specified by point to given color

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        if point.x in range(self.width) and point.y in range(self.height):
            self.pixels[point.y][point.x] = color
        else:
            raise IndexError

    def clear(self):
        ''' Clears Image by setting all pixels to default Color '''
        for y in range(self.height):
            for x in range(self.width):
                self.pixels[y][x] = Color()


    def draw_line(self, point0, point1, color):
        ''' Draw a line from point0 to point1 '''
        dpoint0 = point1.y - point0.y
        dpoint1 = point1.x - point0.x
        angle = math.atan2(dpoint0, dpoint1) 
        distance = point0.distance_from(point1)
        d = 0

        if angle >= math.pi:
            angle = angle - math.pi

        while d <= distance: 
            x = int(point0.x + d*math.cos(angle))
            y = int(point0.y + d*math.sin(angle))
            self.pixels[y][x] = color 
            

            d += .01 

 
    def draw_rectangle(self, point0, point1, color):
        ''' Draw a rectangle from point0 to point1 '''
        xmin = min(point0.x, point1.x)
        xmax = max(point0.x, point1.x)
        ymin = min(point0.y, point1.y)
        ymax = max(point0.y, point1.y)

        for y in range(ymin, ymax):
            for x in range(xmin, xmax):
                self.pixels[y][x] = color


    def draw_circle(self, center, radius, color):
        ''' Draw a circle with center point and radius '''
        xmin = center.x - radius 
        xmax = center.x + radius 
        ymin = center.y - radius 
        ymax = center.y + radius 

        for y in range(ymin, ymax +1):
            for x in range(xmin, xmax +1):
                point = Point(x, y)
                if point.distance_from(center) <= radius: 
                    self.pixels[y][x] = color
        
        



