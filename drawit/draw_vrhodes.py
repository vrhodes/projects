from drawit import Image, Color, Point, PPM, BLACK, WHITE, YELLOW, BLUE, GREEN, RED, MAGENTA, CYAN
import math


image = Image(200, 200)

point2 = Point(75, 75)
point3 = Point(100, 150) 
image.draw_line(point2, point3, YELLOW)

point4 = Point(100, 150)
point5 = Point(125, 75)
image.draw_line(point4, point5, YELLOW)

point6 = Point(140, 150)
point7 = Point(140, 75)
image.draw_line(point6, point7, BLUE)



image.draw_line(Point(140, 90), Point(170, 90), BLUE)

image.draw_line(Point(170, 90), Point(170, 105), BLUE)

PPM.write(image)