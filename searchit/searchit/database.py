#!/usr/bin/env python

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'searchit.db'
    YAML_PATH   = 'assets/yaml/data.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        # TODO: Set instance variables and call _create_tables and _load_tables
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(path)

        #call internal functions
        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        # TODO: Create Artist, Album, and Track tables
        with self.conn:
            cursor = self.conn.cursor()   

            artists_sql = '''
            
            CREATE TABLE IF NOT EXISTS Artists(
                ArtistID  INTEGER  NOT NULL,
                Name      TEXT     NOT NULL,
                Image     TEXT     NOT NULL,
                PRIMARY KEY(ArtistID)
            )
            '''

            albums_sql = '''
        
            CREATE TABLE IF NOT EXISTS Albums(
                ArtistID  INTEGER  NOT NULL,
                AlbumID   INTEGER  NOT NULL,
                Name      TEXT     NOT NULL,
                Image     TEXT     NOT NULL,
                PRIMARY KEY(AlbumID),
                FOREIGN KEY(ArtistID) References Artists(ArtistID)
            )
            '''

        tracks_sql = '''
        
            CREATE TABLE IF NOT EXISTS Tracks(
                AlbumID   INTEGER  NOT NULL,
                TrackID   INTEGER  NOT NULL,
                Number    INTEGER  NOT NULL,
                Name      TEXT     NOT NULL,
                PRIMARY KEY(TrackID),
                FOREIGN KEY(AlbumID) References Albums(AlbumID)
            )
            '''
        cursor.execute(artists_sql)
        self.logger.info('Created Artists Table')

        cursor.execute(albums_sql) 
        self.logger.info('Created Albums Table')   

        cursor.execute(tracks_sql)
        self.logger.info('Created Tracks Table')    


    def _load_tables(self):
        # TODO: Insert Artist, Album, and Track tables from YAML
        with self.conn:
            cursor = self.conn.cursor()
            

            artists_sql = '''
                INSERT OR REPLACE INTO Artists (ArtistID, Name, Image)
                VALUES (?, ?, ?)
                '''
            albums_sql = '''
                INSERT OR REPLACE INTO Albums (ArtistID, AlbumID, Name, Image)
                VALUES (?, ?, ?, ?)
                '''
            tracks_sql = '''
                INSERT OR REPLACE INTO Tracks (AlbumID, TrackID, Number, Name)
                VALUES (?, ?, ?, ?)
                '''

            artist_id = 0
            album_id  = 0
            track_id  = 0


            for index, artist in enumerate(yaml.load(open(self.data))):
                artist_name = artist['name']
                image = artist['image']
                artist_id = index + 1

                cursor.execute(artists_sql, (artist_id, artist_name, image))
                self.logger.debug('Added Artist: id={}, name={}'.format(artist_id, artist['name']))

                for album in artist['albums']:
                    album_name = album['name']
                    album_image = album['image']
                    album_id  += 1 

                    cursor.execute(albums_sql, (artist_id, album_id, album_name, album_image))
                    self.logger.debug('Added Album: albumID={}, name={}'.format(album_id, album['name']))
                    track_number = 0

                    for track in album['tracks']:
                        track_name = track
                        track_id += 1 
                        track_number += 1

                        cursor.execute(tracks_sql, (album_id, track_id, track_number, track_name))
                        self.logger.debug('Added Track: trackID={}, name={}'.format(track_id, track))
        

    def artists(self, artist):
        # TODO: Select artists matching query
        with self.conn:
            cursor = self.conn.cursor()
            artists_sql = '''
            SELECT Artists.ArtistID, Name, Image
                FROM Artists
                WHERE Name LIKE ? ORDER BY Name
            '''
            return cursor.execute(artists_sql, ('%{}%'.format(artist),))

    def artist(self, artist_id=None):
        # TODO: Select artist albums
        with self.conn:
            cursor = self.conn.cursor()
            artists_sql = '''
            SELECT Albums.AlbumID, Name, Image
                FROM Albums
                WHERE ArtistID = ? ORDER BY Name
            '''
            return cursor.execute(artists_sql, (artist_id,))


    def albums(self, album):
        # TODO: Select albums matching query
        with self.conn:
            cursor = self.conn.cursor()
            albums_sql = '''
            SELECT AlbumID, Name, Image
                FROM Albums
                WHERE Name LIKE ? ORDER BY Name
            '''
            return cursor.execute(albums_sql, ('%{}%'.format(album),))

    def album(self, album_id=None):
        # TODO: Select specific album
         with self.conn:
            cursor = self.conn.cursor()
            albums_sql = '''
            SELECT TrackID, Number, Name
                FROM Tracks
                WHERE AlbumID LIKE ? 
            '''
            return cursor.execute(albums_sql, ('%{}%'.format(album_id),))

    def tracks(self, track):
        # TODO: Select tracks matching query
         with self.conn:
            cursor = self.conn.cursor()
            tracks_sql = '''
            SELECT TrackID, Tracks.Name, Albums.Image
                FROM Tracks
                JOIN Albums ON Albums.AlbumID=Tracks.AlbumID 
                WHERE Tracks.Name LIKE ? ORDER BY Tracks.Name
            '''
            return cursor.execute(tracks_sql, ('%{}%'.format(track),))

    def track(self, track_id=None):
        # TODO: Select specific track
        with self.conn:
            cursor = self.conn.cursor()
            tracks_sql = '''
            SELECT TrackID, Artists.ArtistID, Artists.Name, Albums.Name, Albums.AlbumID, Number, Tracks.Name
                FROM Tracks
                JOIN Albums ON Albums.AlbumID=Tracks.AlbumID 
                JOIN Artists ON Artists.ArtistID=Albums.ArtistID
                WHERE TrackID = ? 
            '''
            return cursor.execute(tracks_sql, (track_id,))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
