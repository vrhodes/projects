''' searchit.web - Web Application '''

from database import Database

import logging
import socket
import sys

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9876
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        # TODO: Initialize database and port
        self.logger   = logging.getLogger()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.database = Database() 
        self.port     = port 

        # TODO: Add Index, Album, Artist, and Track Handlers
        self.add_handlers('', (
            (r'/'           , IndexHandler),
            (r'/artist/(.*)', ArtistHandler),
            (r'/album/(.*)' , AlbumHandler),
            (r'/track/(.*)' , TrackHandler),
            (r'/image/(.*)' , tornado.web.StaticFileHandler,{'path':"assets/image"})
            ))

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Implement Index Handler
        query = self.get_argument('query', '')
        table = self.get_argument('table', '')

        if table == 'Artists':
            group = self.application.database.artists(query)
            self.render('gallery.html', group = group, prefix = 'artist')
           
        elif table == 'Albums':
            group = self.application.database.albums(query)
            self.render('gallery.html', group = group, prefix = 'album')

        elif table == 'Tracks':
            group = self.application.database.tracks(query)
            self.render('gallery.html', group = group, prefix = 'track')

        else:
            self.render('index.html')


class ArtistHandler(tornado.web.RequestHandler):
    def get(self, artist_id=None):
        # TODO: Implement Artist Handler
        if artist_id:
            group = self.application.database.artist(artist_id)
            self.render('gallery.html', group=group, prefix = 'album')
        else:
            group = self.application.database.artists('')
            self.render('gallery.html', group=group, prefix = 'artist')

class AlbumHandler(tornado.web.RequestHandler):
    def get(self, album_id=None):
        # TODO: Implement Album Handler
        if album_id:
            group = self.application.database.album(album_id)
            self.render('album.html', group=group, prefix = 'album')
        else:
            group = self.application.database.albums('')
            self.render('gallery.html', group=group, prefix = 'album')

class TrackHandler(tornado.web.RequestHandler):
    def get(self, track_id=None):
        # TODO: Implement Album Handler
        if track_id:
            track = self.application.database.track(track_id)
            self.render('track.html', group=track, prefix = 'track')
        else:
            group = self.application.database.tracks('')
            self.render('gallery.html', group=group, prefix = 'track')

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
