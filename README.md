Projects
========

1. See photos

2. How does your proposed project meet the requirements above?
	- Uses Tornado because it's a website and that's the server we're using. We have to store our animal data and we'll do this with SQL. Bootstrap will allow us to make a beautiful theme for our children's website. JavaScript is used to make interactive tabs and a search bar. AJAX and JSON will be used so we don't have to update the entire page over and over again. 

3. What will each person's role or responsibility be in the project?
	- Indi: responsible for databases and JavaScript
	- Virginia: responsible for Tornado and HTML layout 

4. What questions or issues do you need addressed before implementing the project?
	- None, we good. 
